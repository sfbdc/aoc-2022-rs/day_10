use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let input = include_str!("../input.txt");
    println!("Solution A: {}", solve_a(input)?);
    println!("Solution B:");
    println!("{}", solve_b(input)?);
    Ok(())
}

fn solve_a(input: &str) -> Result<u32, Box<dyn Error>> {
    let mut cycle: u32 = 1;
    let mut x: i32 = 1;
    let mut machine_state = vec![(1u32, 1i32)];

    for line in input.lines() {
        match &line[0..4] {
            "addx" => {
                cycle += 1;
                machine_state.push((cycle, x));
                x += &line[5..].parse::<i32>()?;
            }
            "noop" => {}
            _ => {}
        }
        cycle += 1;
        machine_state.push((cycle, x));
    }

    let signal_sum: u32 = machine_state
        .iter()
        .skip(19)
        .step_by(40)
        .fold(0, |acc, (cycles, x)| acc + cycles * (*x as u32));

    Ok(signal_sum)
}

fn solve_b(input: &str) -> Result<String, Box<dyn Error>> {
    let mut cycle: i32 = 1;
    let mut x: i32 = 1;
    let mut machine_state = vec![(1i32, 1i32)];

    for line in input.lines() {
        match &line[0..4] {
            "addx" => {
                cycle += 1;
                machine_state.push((cycle, x));
                x += &line[5..].parse::<i32>()?;
            }
            "noop" => {}
            _ => {}
        }
        cycle += 1;
        machine_state.push((cycle, x));
    }

    let mut out = machine_state
        .iter()
        .map(|(cycle, x)| {
            let opt_n = if (cycle) % 40 == 0 { "\n" } else { "" };
            if ((cycle - 1) % 40).abs_diff(*x) < 2 {
                format!("#{}", opt_n)
            } else {
                format!(".{}", opt_n)
            }
        })
        .collect::<String>();
    out.truncate(out.len() - 2);

    Ok(out)
}

#[test]
fn test_solve_a() {
    let input = include_str!("../test.txt");
    assert_eq!(13140, solve_a(input).unwrap());
}

#[test]
fn test_solve_b() {
    let input = include_str!("../test.txt");
    assert_eq!(
        solve_b(input).unwrap(),
        String::from(
            "##..##..##..##..##..##..##..##..##..##..
###...###...###...###...###...###...###.
####....####....####....####....####....
#####.....#####.....#####.....#####.....
######......######......######......####
#######.......#######.......#######....."
        )
    );
}
